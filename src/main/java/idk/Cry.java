package idk;

import java.util.*;
public class Cry {
    static String t;
    static String[] slot;
    static boolean win;
    static String winner;
    
    static void welcome(){
        System.out.print("Welcome to XO! \n" );
    }
    static void tablecontrol(){
        slot = new String[10];
        for (int i = 1; i < 10; i++) {
            slot[i] = String.valueOf(i);
        }
    }
    static void table(){
        System.out.println(slot[7]+" "+slot[8]+" "+slot[9]);
        System.out.println(slot[4]+" "+slot[5]+" "+slot[6]);
        System.out.println(slot[1]+" "+slot[2]+" "+slot[3]);
    }
    static void turn(){
        if(t == "X"){
            t = "O";
        }else{
            t = "X";
        }
        System.out.println(t + " turn");
    }
    static void inputandcheck(){
        String input;
        boolean caught = true;
        Scanner ss = new Scanner(System.in);
        while(caught == true){
            System.out.print("Please pick a slot with your numpad : ");
            input = ss.next();
            
            try {
                int inp = Integer.parseInt(input);
                if (!(inp >= 1 && inp <= 9)) {
                System.out.print("Invalid input. Please try again. \n");
                }else if(slot[inp].equals(Integer.toString(inp))){
                    slot[inp]= t;
                    caught = false;
                }else{
                    System.out.println("Slot is taken. Please try again.");
                }
            }catch (NumberFormatException e) {
                System.out.print("Invalid input. Please try again. \n");
                continue;
            }
            
        }
    }
    static String state(){
        for (int c = 0; c < 8; c++) {
            switch (c) {
                case 0:
                    if(slot[1] == slot[2] && slot[2]== slot[3]){
                        win = true;
                    }
                    break;
                case 1:
                    if(slot[4] == slot[5] && slot[5] == slot[6]){
                        win = true;
                    }
                    break;
                case 2:
                    if(slot[7] == slot[8] && slot[8] == slot[9]){
                        win = true;
                    }
                    break;
                case 3:
                    if(slot[1] == slot[4] && slot[4] == slot[7]){
                        win = true;
                    }
                    break;
                case 4:
                    if(slot[2] == slot[5] && slot[5] == slot[8]){
                        win = true;
                    }
                    break;
                case 5:
                    if(slot[3] == slot[6] && slot[6] == slot[9]){
                        win = true;
                    }
                    break;
                case 6:
                    if(slot[1] == slot[5] && slot[5] == slot[9]){
                        win = true;
                    }
                    break;
                case 7:
                    if(slot[3] == slot[5] && slot[5] == slot[7]){
                        win = true;
                    }
                    break;
            }
            if (win == true && t == "X") {
                return "Congratulations! X wins!";
            }
            else if (win == true && t == "O") {
                return "Congratulations! O wins!";
            }
        }
        for (int i = 0; i < 9; i++) {
            if (Arrays.asList(slot).contains(String.valueOf(i))) {
                break;
            }
            else if (i == 8) {
                return "It’s a draw!";
            }
        }
        return null;
    }
    public static void main(String[] args) {
        String end = null;
        welcome();
        tablecontrol();          
        while (end == null){
            table();
            turn(); 
            inputandcheck();
            end = state();
        }
        table();
        System.out.println(state());
        System.out.println(slot[0]);
    }
}
